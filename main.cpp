#include "main.h"

SAMPFUNCS *SF = new SAMPFUNCS();


bool __stdcall raknet_hook(stRakNetHookParams* params) {
		
	if (params->packetId == ID_BULLET_SYNC) {

		BitStream *bs = params->bitStream;

		stBulletData data;

		bs->ResetReadPointer();
		bs->IgnoreBits(8);
		bs->Read((PCHAR)&data, sizeof(stBulletData));
		bs->ResetReadPointer();

		if (data.byteType == 0 || data.byteType > 2) {

			*reinterpret_cast<int*>(0x969178) = 1;

			return false;
		}
		
		*reinterpret_cast<int*>(0x969178) = 0;
	}
	return true;
}

void __stdcall mainloop() {

	static bool initialized = false;

	if (!initialized) {

		if (GAME && GAME->GetSystemState() == eSystemState::GS_PLAYING_GAME && SF->getSAMP()->IsInitialized()) {

			initialized = true;
			
			SF->getRakNet()->registerRakNetCallback(RakNetScriptHookType::RAKHOOK_TYPE_OUTCOMING_PACKET, raknet_hook);
		}
	}
}

int __stdcall DllMain(HMODULE hModule, DWORD dwReasonForCall, LPVOID lpReserved) {

	if (dwReasonForCall == DLL_PROCESS_ATTACH)
		SF->initPlugin(mainloop, hModule);

	return true;
}
